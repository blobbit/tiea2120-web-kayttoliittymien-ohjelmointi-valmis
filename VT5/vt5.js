"use strict";

// funktio annettu tehtavanannossa
function rainbow(numOfSteps, step) {
    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    let r, g, b;
    let h = step / numOfSteps;
    let i = ~~(h * 6);
    let f = h * 6 - i;
    let q = 1 - f;
    switch(i % 6){
        case 0: r = 1; g = f; b = 0; break;
        case 1: r = q; g = 1; b = 0; break;
        case 2: r = 0; g = 1; b = f; break;
        case 3: r = 0; g = q; b = 1; break;
        case 4: r = f; g = 0; b = 1; break;
        case 5: r = 1; g = 0; b = q; break;
    }
    let c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
    return (c);
}

function getLatLonByRastiID(id) {
    for (let rasti of data.rastit)
	if (rasti.id == id) return [rasti.lat, rasti.lon];
}

function getJoukkueByID(id) {
    for (let joukkue of data.joukkueet)
	if (joukkue.id == id) return joukkue;
}

function getReitti(joukkue) {
    var leimaukset = joukkue.rastit;
    
    // sortataan leimaukset aikajarjestykseen
    leimaukset.sort(function (a, b) {
	function stringToTime(str) {
	    return new Date(str.replace(' ', 'T')).getTime();
	}
	return stringToTime(a.aika) - stringToTime(b.aika);
    });

    var latlons = [];
    for (let leimaus of leimaukset) {
	const latlon = getLatLonByRastiID(leimaus.id);
	if (latlon == undefined) continue;
	latlons.push(latlon);
    }

    return latlons;
}

$( document ).ready(function() {
    const joukkueet = data.joukkueet;
    const joukkueetList = document.getElementById('joukkueetList');

    for (let i = 0; i < joukkueet.length; i++) {
	const joukkue = joukkueet[i];

	let li = document.createElement('li');
	li.style.backgroundColor = rainbow(joukkueet.length, i);
	li.appendChild(document.createTextNode(joukkue.nimi));
	joukkueetList.appendChild(li);
	li.id = joukkue.id.toString();

	li.setAttribute('draggable', 'true');
	li.addEventListener('dragstart', function(e) {
	    e.dataTransfer.setData('text/plain', li.id); // msdn dokumentaation mukaan kannattaa kayttaa tekstia jos mahdollista (vaikka ei silla varmaan tassa ole hirveesti valia)
	    e.dataTransfer.setData('color', li.style.backgroundColor);
	});
    }
    
    const kartalla = document.getElementById('kartalla');
    kartalla.addEventListener('dragover', function(e) {
	e.preventDefault();
	e.dataTransfer.dropEffect = 'move';
    });

    kartalla.addEventListener('drop', function(e) {
	e.preventDefault();
	
	const draggedID = e.dataTransfer.getData('text');
	const draggedElement = document.getElementById(draggedID);
	const list = document.getElementById('kartallaList');
	list.insertBefore(draggedElement, list.childNodes[0]);

	const joukkue = getJoukkueByID(draggedID);
	if (joukkue == undefined) return;

	var polyline = L.polyline(getReitti(joukkue), {color: e.dataTransfer.getData('color')}).addTo(map);
	polyline.bringToFront();
	// TODO: tallenna polyline + viite taulukkoon, kun siirretaan joukkue pois kartalta niin poistetaan polyline myos
    });
    
    // TODO alusta tama eka
    let map = L.map('kartta', {
	crs: L.TileLayer.MML.get3067Proj()
    }).setView([62.2333, 25.7333], 11);
    L.tileLayer.mml_wmts({ layer: 'maastokartta' }).addTo(map);

    
    var smallestLat = 90, biggestLat = 0, smallestLon = 180, biggestLon = 0; // 90 is the biggest possible latitude and 180 the biggest possible longetivity

    for (let rasti of data.rastit) {
	const lat = rasti.lat, lon = rasti.lon;

	let circle = L.circle(
        [lat, lon], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 150
        }
        ).addTo(map);
	
	// let's get the outmost bounds
	if (lat < smallestLat) smallestLat = lat;
	else if (lat > biggestLat) biggestLat = lat;

	if (lon < smallestLon) smallestLon = lon;
	else if (lon > biggestLon) biggestLon = lon;
    }

    map.fitBounds([[smallestLat, smallestLon],
		   [biggestLat, biggestLon]]);
});
