// data-muuttuja sisältää kaiken tarvittavan ja on rakenteeltaan lähes samankaltainen kuin viikkotehtävässä 2
// Rastileimaukset on siirretty tupa-rakenteesta suoraan jokaisen joukkueen yhteyteen
// Rakenteeseen on myös lisätty uusia tietoja
// voit tutkia tarkemmin käsiteltävää tietorakennetta konsolin kautta 
// tai json-editorin kautta osoitteessa http://jsoneditoronline.org/
// Jos käytät json-editoria niin avaa data osoitteesta:
// http://appro.mit.jyu.fi/tiea2120/vt/vt3/data.json

"use strict";

console.log(data);

/*
 * EN: I write all of the general functions (as in what I can use in other projects) in english.
 * FIN: Kirjoitan kaikki yleishyodylliset funktiot (eli mita voi kayttaa joskus muissa projekteissa) englanniksi.
 */

// creates and returns an element and appends it to the parent
function createElement(parent, elementName) {
    var element = document.createElement(elementName);
    parent.appendChild(element);
    return element;
}

// adds a text node to the element
function addText(element, txt) {
    element.appendChild(document.createTextNode(txt));
}

// general function to extract fields from array
function fieldsFromArray(array, field) {
    var fields = [];

    for (const i in array)
        fields.push(array[i][field]);

    return fields;
}

// checks if given element has children and removes them
function checkAndRemoveChildren(parent) {
    if (parent == null || parent == undefined) return;
    if (parent.hasChildNodes())
        for (let i = parent.childNodes.length - 1; i >= 0; i--) parent.childNodes[i].remove();
}

// creates html label + input list elements from string array
function createInputList(parentID, array, valueArray, type, value) {
    var parent = document.getElementById(parentID);
    checkAndRemoveChildren(parent);
    var inputs = [];

    let j = 0;
    for (let i of array) {
        var li = createElement(parent, 'li');

        var input = createInputWithLabel(li, i + ' ', type, parentID + i.toLowerCase());

        input.name = parentID;
        inputs.push(input);
        input.value = valueArray[j++];
    }
    return inputs;
}

// creates an input field and a label. Attaches label to the input field.
function createInputWithLabel(parent, labelText, inputType, id) {
    var label = createElement(parent, 'label');
    addText(label, labelText);

    var input = createElement(parent, 'input');
    input.type = inputType;
    label.htmlFor = input.id = id;

    return input;
}


/* Lisataan kaikille sarjan vaihto radio buttoneille eventti joka
 * paivittaa leimausajan vaatimukset
 * 
 * ilman tata kayttaja voisi tehda toisessa sarjassa leimausajan, vaihtaa sarjaa ja leimausaika olisi vielakin validi
 * 
 * Rastileimausaika saa olla vain sarjan alku- ja loppuajan väliltä.
 * Jos sarjalle ei ole annettu alku- tai loppuaikaa, niin käytetään kilpailun alku- ja loppuaikaa.
 */
function sarjanVaihtoPaivittaaLeimaustenVaatimukset(sarjatInputs) {
    var leimausajatInputs = document.getElementsByClassName('leimausaika');

    for (let i of sarjatInputs) {
        var sarja = getRastiByID(i.value);
        i.onchange = function () {
            for (let j = 0; j < leimausajatInputs.length-1; j++) { // ei oteta vikaa mukaan se on tyhja
                validateLeimausAika(leimausajatInputs[j], sarja)
            }
        }
    }
}

// rastileimausaika saa olla vain sarjan alku- ja loppuajan valilta. 
// jos sarjalle ei ole annettu alku - tai loppuaikaa, niin käytetään kilpailun alku - ja loppuaikaa.
function validateLeimausAika(input, sarja) {
    var alku, loppu, aika = dateToMs(input.value);

    if (sarja.alkuaika != null && sarja.loppuaika != null) {
        alku = dateToMs(sarja.alkuaika);
        loppu = dateToMs(loppu.alkuaika);
    } else {
        alku = dateToMs(KISA.alkuaika);
        loppu = dateToMs(KISA.loppuaika);
    }

    if (alku < aika && aika < loppu) input.setCustomValidity('');
    else input.setCustomValidity('Rastileimausaika saa olla vain sarjan alku- ja loppuajan väliltä!');
}

// jos annetaan joukkue luo listauksen joukkueen rastileimauksista
// poistetaan vanha listaus jos on
// jos ei anneta joukkuetta luodaan vain yksi tyhja rastileimaus
function paivitaRastileimaukset(joukkue) {
    var vanha = document.getElementById('rastileimausTaulukko');
    if (vanha != null) {
        checkAndRemoveChildren(vanha);
        vanha.remove();
    }

    var rastit = [];
    if (joukkue != undefined)
        rastit = joukkue.rastit;

    rastit.sort(sortSarjaAjanMukaan);

    function onkoLeimattu(id) {
        for (let i of rastit)
            if (i.id == id) return true;
        return false;
    }

    var datalist = document.getElementById('rastiLista');
    for (let i of data.rastit) {
        if (!onkoLeimattu(i.id)) {
            let option = createElement(datalist, 'option');
            option.value = i.koodi;
        }
    }

    var headers = new Map([['Rasti', teeRastiValinta], ['Aika', teeAikaInput], ['Poista', teeRastinPoistoInput]]);
    var table = createTableFromArray(headers, rastit);
    table.id = 'rastileimausTaulukko';
    document.getElementById('rastileimaukset').appendChild(table);
    lisaaTyhjaLeimaus(table, headers); // yksi tyhja aluksi

    var aikaInputs = document.getElementsByClassName('leimausaika');
    for (let input of aikaInputs) {
        input.onblur = function () {
            validateLeimausAika(input, getSarja());
            var leimausajat = document.getElementsByClassName('leimausaika');

            for (let i of leimausajat)
                if (i.value == '') return;

            lisaaTyhjaLeimaus(table, headers);
        }
    }
    
    return rastit;
}

// tekee tyhjan rastileimauksen (jotta kayttaja voi lisata uusia rastileimauksia)
function lisaaTyhjaLeimaus(table, headers) {
    var tr = createElement(table, 'tr');
    for (let func of headers.values()) {
        var td = createElement(tr, 'td');
        func(td, { aika:'', id:'' });
    }
}

// tekee rastileimauksen poisto solun
function teeRastinPoistoInput(parent, rasti) {
    var input = createElement(parent, 'input');
    input.type = 'checkbox';
    input.className = 'leimauksenPoisto';
}

// tekee rastileimauksen ajan valinta solun
function teeAikaInput(parent, rasti) {
    var input = createElement(parent, 'input');
    input.type = 'datetime-local';
    input.step = '1';
    input.className = 'leimausaika';
    input.value = dateStringToISO(rasti.aika);
}

// tekee rastileimauksen rastin valinta solun
function teeRastiValinta(parent, rasti) {
    var input = createElement(parent, 'input');
    input.value = getRastiByID(rasti.id).koodi;
    input.setAttribute('list', 'rastiLista');
    input.className = 'rastinValinta';
    input.onblur = function () {
        var rastiValinnat = document.getElementsByClassName('rastinValinta');
        let j = 0;
        for (let i of rastiValinnat)
            if (input.value == i.value) j++;
        if (j > 1) {
            input.setCustomValidity('Samaa rastia ei saa leimata kahdesti!');
            return;
        } else
            input.setCustomValidity('');

        for (let i of data.rastit)
            if (input.value == i.koodi) {
                input.setCustomValidity('');
                return;
            }
        input.setCustomValidity('Virheellinen rastikoodi!');
    }
}

/* creates a html table out from array 'objects'.
 * 
 * headers is a Map where each object has 
 *  Key     - returns header name
 *  Value   - returns function for creating the cell
 */
function createTableFromArray(headers, objects) {
    var table = document.createElement('table');

    var tr = createElement(table,'tr');

    // headers
    for (let header of headers.keys()) {
        let th = createElement(tr, 'th');
        addText(th, header);
    }

    // rows
    for (let i = 0; i < objects.length; i++) {
        tr = createElement(table,'tr');
        // columns
        for (let func of headers.values()) {
            var td = createElement(tr,'td');
            func(td, objects[i]);
        }
    }

    return table;
}

// tekee uudet jasen label + input kentat
function teeJasenetListaus() {
    var inputs = [];
    for (let i = 0; i < 5; i++)
        lisaaJasenKentta(i, inputs);
    return inputs;
}

function lisaaJasenKentta(i, inputs) {
    var parent = document.getElementById('jasenetListaus');
    var div = createElement(parent, 'div');
    var input = createInputWithLabel(div, 'Jäsen ' + (i + 1), 'text', 'jasen ' + i);
    input.className = 'kentta';
    inputs.push(input);
    return input;
}

// validoi jasen input kentat
function tarkistaJasenet(inputs) {
    for (var i = 0, j = 0; i < inputs.length; i++) {
        if (inputs[i].value != '') {
            j++;
            for (let k = i+1; k < inputs.length; k++) {
                if (inputs[i].value == inputs[k].value) {
                    inputs[i].setCustomValidity('Ei saa olla samannimisiä jäseniä!');
                    return;
                }
            }
        }
    }
    if (j >= 2) {
        for (let i of inputs) i.setCustomValidity('');
    } else inputs[j].setCustomValidity('Jäseniä oltava vähintään 2!');
}

function lisaaAutomaattisestiJasenKenttia(inputs) {
    for (let i of inputs) {
        i.onblur = function () {
            let k = 0;
            for (let j of inputs) {
                if (j.value == '') return;
                k++;
            }
            lisaaJasenKentta(k, inputs).onblur = i.onblur;
        }
    }
}

// (poistaa vanhan joukkue listauksen jos on) ja tekee uuden
function paivitaJoukkueListaus(nimiInput, luontiaikaInput, leimausInputs, jasenetInputs) {
    var parent = document.getElementById('joukkueListaus');
    if (parent.hasChildNodes())
        parent.childNodes[0].remove();

    var lista = createElement(parent, 'ul');

    let joukkueetSorted = data.joukkueet.sort(sortByAttr('nimi'));
    for (let i of joukkueetSorted) {
        let li = createElement(lista, 'li');
        li.className = 'joukkue';
        let a = createElement(li, 'a');
        addText(a, i.nimi);
        a.href = '#joukkueForm';
        a.onclick = formiinMuokattavanJoukkueenTiedot(i, nimiInput, luontiaikaInput, leimausInputs, jasenetInputs);

        if (i.jasenet == undefined) continue;
        let ul = createElement(li, 'ul');
        for (let j of i.jasenet.sort()) {
            let li = createElement(ul, 'li');
            addText(li, j);
        }
    }

    return lista;
}

function formiinMuokattavanJoukkueenTiedot(joukkue, nimiInput, luontiaikaInput, leimausInputs, jasenetInputs) {
    return function () {
        document.getElementById('joukkueForm').reset();

        nimiInput.value = joukkue.nimi;
        luontiaikaInput.value = dateStringToISO(joukkue.luontiaika);
        currentJoukkueID = joukkue.id;

        for (let i of leimausInputs)
            for (let j of joukkue.leimaustapa)
                if (i.value == j) i.checked = true;

        hasOneInputChecked(leimausInputs);

        for (let i of document.getElementsByName('sarjat'))
            if (i.value == joukkue.sarja) i.checked = true;

        for (let i = 0; i < joukkue.jasenet.length; i++) {
            jasenetInputs[i].value = joukkue.jasenet[i];
            jasenetInputs[i].setCustomValidity('');
        }

        paivitaRastileimaukset(joukkue);
    }
}

// returns a sort function to sort by given attribute
function sortByAttr(attr) {
    return function (a, b) { return sort(a[attr], b[attr]) };
}

function sortSarjaAjanMukaan(a, b) {
    return sort(dateToMs(dateStringToISO(b.aika)), dateToMs(dateStringToISO(a.aika)));
}

// general sort algorithm to sort strings or numbers
// sorts strings in ascending order (alphabetical)
// sorts numbers in descending order
function sort(a, b) {
    if (typeof a == 'string' && typeof b == 'string') {
        var aa = a.toLowerCase(),
            bb = b.toLowerCase();

        if (aa < bb) return -1;
        if (aa > bb) return 1;
        return 0;
    } else return b - a;
}

// goes through all inputs in the given array, returns true if there is atleast one of them checked
function hasOneInputChecked(inputs) {
    for (let i of inputs)
        if (i.checked) {
            for (let j of inputs)
                j.setCustomValidity('');
            return;
        }
    return inputs[0].setCustomValidity('Valittava vähintään yksi leimaustapa!');
}

// for validating inputs. Validates via validateFunc everytime given event is called
function customRequirement(inputs, event, validateFunc) {
    validateFunc(inputs); // initial validation
    for (let i of inputs)
        i.addEventListener(event, function () { validateFunc(inputs); }); // 
}

/* alkuaika ja loppuaika saavat olla tyhjia
 * alkuaika ei saa olla pienempi kuin kilpailun alkuaika
 * loppuaika ei saa olla suurempi kuin kilpailun loppuaika
 * loppuaika on oltava suurempi kuin kyseisen sarjan alkuaika.
 * loppuaika on oltava suurempi kuin kyseisen sarjan alkuaika + kesto
 */
function validateAlkuJaLoppuAika(kesto, alkuInput, loppuInput) {
    var kisa = KISA;

    function tarkistaAika(value, kisaAika, tarkFunc, input, errorMessage) {
        if (value != '') {
            var aika = new Date(value).getTime();
            let kisaDate = new Date(kisaAika);
            let kisa = kisaDate.getTime();

            if (tarkFunc(aika, kisa)) return input.setCustomValidity(errorMessage + ' (' + kisaDate.toLocaleString() + ')');
            else input.setCustomValidity('');

            return aika;
        } else
            input.setCustomValidity('');
    }

    let alku =  tarkistaAika(alkuInput.value, kisa.alkuaika, function (a, b) { return a < b; }, alkuInput, 'Alkuaika ei saa olla pienempi kuin kilpailun alkuaika!'),
        loppu = tarkistaAika(loppuInput.value, kisa.loppuaika, function (a, b) { return a > b; }, loppuInput, 'Loppuaika ei saa olla suurempi kuin kilpailun loppuaika!');

    if (alkuInput.value == '' || loppuInput.value == '') return;

    if (!(loppu > alku)) return loppuInput.setCustomValidity('Loppuajan pitää olla suurempi kuin alkuaika!');
    else loppuInput.setCustomValidity('');

    // 3600000 = 60 (tunnit) * 60 (sekunnit) * 1000 (millisekunnit)
    if (kesto.checkValidity())
        if (!(loppu > (alku + parseInt(kesto.value) * 3600000))) return loppuInput.setCustomValidity('Loppuajan pitää olla suurempi kuin alkuaika + kesto!');
        else loppuInput.setCustomValidity('');
}

// if input value is invalid, resets the field
function resetTimeInputIfInvalidValue(input) {
    input.onblur = function () { if (input.value == '') input.value = ''; }
}

// palauttaa random id:n mikali muokattavana ei juuri nyt ole id:ta
function getJoukkueID(checkFunc) {
    if (currentJoukkueID == null) return generoiID(checkFunc);
    else return currentJoukkueID;
}

// generoi uuden id:n
// checkfunc on kaytannossa turha koska uuden id:n mahdollisuuksia on niin paljon mutta tulipahan tehtya
function generoiID(checkFunc) {
    let random;
    while (checkFunc(random = parseInt(Math.random() * Number.MAX_SAFE_INTEGER)));
    return random;
}

// tarkistaa onko jollain joukkueella jo sama id luotaessa uutta joukkuetta
function checkIDJoukkue(random) {
    for (let i of data.joukkueet)
            if (i.id == random) return true;
    return false;
}

// tarkistaa onko jollain sarjalla jo sama id luotaessa uutta sarjaa
function checkIDSarja(random) {
    for (let i of KISA.sarjat)
        if (i.id == random) return true;
    return false;
}

// sarjan ja joukkueen nimen validointi on periaatteessa sama homma joten tein niille yhteisen funktion
function validateOnkoSamaNimi(nimiInput, array) {
    for (let i of array)
        if (i.nimi.trim() == nimiInput.value.trim()) {
            nimiInput.setCustomValidity(nimiInput.value + ' on jo olemassa!');
            return;
        }
    nimiInput.setCustomValidity('');
}

// palauttaa valitut leimaustavat taulukossa
function getLeimaustavat(inputs) {
    var leimaustavat = [];

    for (let i of inputs)
        if (i.checked) leimaustavat.push(i.value);

    return leimaustavat;
}

// palauttaa valitun sarjan
function getSarja() {
    var inputs = document.getElementsByName('sarjat');

    for (let i of inputs)
        if (i.checked) return i.value;
}

// palauttaa id:ta vastaavan rastin. Jos ei loydy niin palauttaa tyhjan rastin
function getRastiByID(id) {
    for (let i of data.rastit)
        if (i.id == id) return i;
    return { koodi: '' };
}

// palauttaa koodia vastaavan rastin id:n. Jos ei loydy niin palauttaa tyhjan
function getRastiIDByKoodi(koodi) {
    for (let i of data.rastit)
        if (i.koodi == koodi) return i.id;
    return '';
}

// palauttaa jasenet taulukossa
function getJasenet(inputs) {
    var jasenet = [];
    for (let i of inputs)
        if (i.value != '') jasenet.push(i.value);
    return jasenet;
}

// palauttaa daten tietorakenteen muodossa (vvvv-kk-pp hh:mm) HUOM. tietorakenne ei käytä ISO-standardia
// mikali date on tyhja palauttaa null
function dateISOToString(date) {
    if (date == '') return null;
    return date.substring(0, 10) + ' ' + date.substring(11, 16);
}

// converttaa daten tietorakenteen muodosta (vvvv-kk-pp hh:mm) ISO-standardiin
function dateStringToISO(str) {
    return (str).replace(' ', 'T');
}

function dateToMs(date) {
    return new Date(date).getTime();
}

/* finds element by the given id
 * removes children if has any
 * creates a text node with given text to the element
 */ 
function updateLabel(id, text) {
    let i = document.getElementById(id);
    checkAndRemoveChildren(i);
    addText(i, text);
}

// jos id:lla loytyy joukkue, niin muokataan vanhaa
// palautetaan true jos muokataan
function muokataankoJoukkuetta(uusiJoukkue) {
    for (let j of data.joukkueet) {
        if (j.id == uusiJoukkue.id) {
            j.nimi = uusiJoukkue.nimi;
            j.jasenet = uusiJoukkue.jasenet;
            j.sarja = uusiJoukkue.sarja;
            j.leimaustapa = uusiJoukkue.leimaustapa;
            j.luontiaika = uusiJoukkue.luontiaika;
            j.rastit = uusiJoukkue.rastit;
            return true;
        }
    }
    return false;
}


// funktio vastaa joukkue formin luomisesta
function alustaLisaaJoukkueForm() {
    var leimausInputs = createInputList('leimaustavat', KISA.leimaustapa, KISA.leimaustapa, 'checkbox', '');
    var sarjatInputs = createInputList('sarjat', fieldsFromArray(KISA.sarjat, 'nimi'), fieldsFromArray(KISA.sarjat, 'id'), 'radio', 'id');
    sarjatInputs[0].checked = true;  // valitaan 1. optio oletuksena

    customRequirement(leimausInputs, 'change', hasOneInputChecked);

    var nimi = document.getElementById('nimi');
    nimi.oninput = function () { validateOnkoSamaNimi(nimi, data.joukkueet); }

    var luontiaika = document.getElementById('luontiaika');
    var pieninDate = new Date('2018-01-01T01:00');
    luontiaika.onchange = function () {
        var d = new Date(luontiaika.value);
        if (d.getTime() > pieninDate.getTime())
            luontiaika.setCustomValidity('Luontiajan on oltava pienempi kuin ' + pieninDate.toLocaleString());
        else
            luontiaika.setCustomValidity('');
    }

    var jasenetInputs = teeJasenetListaus();
    customRequirement(jasenetInputs, 'blur', tarkistaJasenet);
    lisaaAutomaattisestiJasenKenttia(jasenetInputs);

    paivitaJoukkueListaus(nimi, luontiaika, leimausInputs, jasenetInputs);

    sarjanVaihtoPaivittaaLeimaustenVaatimukset(sarjatInputs);
    paivitaRastileimaukset();

    var form = document.getElementById('joukkueForm');
    form.onsubmit = function (e) {
        e.preventDefault();

        var poistoInputs = document.getElementsByClassName('leimauksenPoisto');
        var poistettavat = []; // otetaan poistettavien indeksit talteen
        for (let i = 0; i < poistoInputs.length; i++)
            if (poistoInputs[i].checked) poistettavat.push(i);

        var poistetaanko = false;
        if (poistettavat.length > 0)
            poistetaanko = window.confirm('Haluatko todella poistaa ' + poistettavat.length + ' rastileimausta?');

        var rastit = [];
        let rastinValintaInputs = document.getElementsByClassName('rastinValinta');
        let rastinAikaInputs = document.getElementsByClassName('leimausaika');

        for (let i = 0; i < poistoInputs.length; i++) {
            let aika = rastinAikaInputs[i].value;
            if (aika == '') continue;
            if (poistetaanko && poistettavat.includes(i)) continue;

            rastit.push({ aika: dateISOToString(aika), id: getRastiIDByKoodi(rastinValintaInputs[i].value) });
        }

        const uusiJoukkue = {
            nimi: nimi.value,
            jasenet: getJasenet(jasenetInputs),
            sarja: getSarja(),
            id: getJoukkueID(checkIDJoukkue),
            rastit: rastit,
            leimaustapa: getLeimaustavat(leimausInputs),
            luontiaika: dateISOToString(luontiaika.value)
        }

        if (!muokataankoJoukkuetta(uusiJoukkue)) {
            data.joukkueet.push(uusiJoukkue);
            updateLabel('joukkueLisatty', 'Joukkue ' + uusiJoukkue.nimi + ' on lisätty.');
        } else
            updateLabel('joukkueLisatty', 'Joukkuetta ' + uusiJoukkue.nimi + ' on muokattu.');

        // resetoi formin alkuperaiseen kuntoon
        paivitaJoukkueListaus(nimi, luontiaika, leimausInputs, jasenetInputs);
        paivitaRastileimaukset();
        form.reset();
        sarjatInputs[0].checked = true;
    }
}

// funktio vastaa sarja formin luomisesta
function alustaLisaaSarjaForm() {
    var nimi = document.getElementById('nimi_sarja');
    nimi.oninput = function () { validateOnkoSamaNimi(nimi, KISA.sarjat); }

    var kesto = document.getElementById('kesto');
    kesto.onchange = function () {
        var k = parseFloat(kesto.value);
        kesto.setCustomValidity('');
        if (Number.isInteger(k)) {
            if (k < 1) kesto.setCustomValidity('Keston taytyy olla suurempi kuin 0!');
        } else
            kesto.setCustomValidity('Keston taytyy olla kokonaisluku!');
    }

    var alkuaika = document.getElementById('alkuaika');
    var loppuaika = document.getElementById('loppuaika');

    /* tassa pitaa huomoida se etta time tyyppiset input fieldit antavat valuen vasta kun siina on valid date
     * eli esim. 01-Feb-2018 05:--:-- palauttaa null VAIN koska tunnit puuttuu...
     * ainoa jarkeva tapa kiertaa tama olisi kayttaa tekstipohjaisia input kenttia ja itse parsia aika kayttajan syotteesta.
     * mika taas olisi hirvea tyomaa...
     * en myoskaan loytanyt et miten input kenttien 'virheelliseen' dataan paasis kasiksi
     * tein nyt nain et jos kayttaja jattaa kenttaan virheellisen arvon niin ohjelma yksinkertaisesti resettaa kentan.
     * mun mielesta ihan jarkeva ratkaisu
     */
    alkuaika.onchange = loppuaika.onchange = function () { validateAlkuJaLoppuAika(kesto, alkuaika, loppuaika); }
    resetTimeInputIfInvalidValue(alkuaika);
    resetTimeInputIfInvalidValue(loppuaika);

    var form = document.getElementById('sarjaForm');
    form.onsubmit = function (e) {
        e.preventDefault();

        const uusiSarja = {
            nimi: nimi.value,
            kesto: parseInt(kesto.value),
            loppuaika: dateISOToString(loppuaika.value),
            alkuaika: dateISOToString(alkuaika.value),
            id: generoiID(checkIDSarja)
        }

        KISA.sarjat.push(uusiSarja);

        updateLabel('sarjaLisatty', 'Sarja ' + uusiSarja.nimi + ' on lisätty.');

        let sarjatInputs = createInputList('sarjat', fieldsFromArray(KISA.sarjat, 'nimi'), fieldsFromArray(KISA.sarjat, 'id'), 'radio', 'id');
        sarjatInputs[0].checked = true;
        sarjanVaihtoPaivittaaLeimaustenVaatimukset(sarjatInputs);
        
        form.reset();
    }
}

var currentJoukkueID = null;
var KISA = data.kisat[0];
window.onload = function () {
    alustaLisaaJoukkueForm();
    alustaLisaaSarjaForm();
}