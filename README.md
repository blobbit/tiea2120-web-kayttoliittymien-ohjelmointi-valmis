## Kurssilla opeteltiin web-käyttöliittymien ohjelmointia.

### VT1: Javascript-perusteet (taso5)
http://users.jyu.fi/~aneiakja/TIEA2120/VT1/pohja.html

### VT2: DOM ja tapahtumankäsittely (taso5)
http://users.jyu.fi/~aneiakja/TIEA2120/VT2/pohja.html

### VT3: Lomakkeet ja validointi (taso5)
http://users.jyu.fi/~aneiakja/TIEA2120/VT3/pohja.html

### VT4: Canvas, SVG ja animaatiot (taso5)
http://users.jyu.fi/~aneiakja/TIEA2120/VT4/pohja.html

### VT5: jQuery, Drag & Drop, kartat (taso1)
http://users.jyu.fi/~aneiakja/TIEA2120/VT5/pohja.html

### VT6: React (taso5)
http://users.jyu.fi/~aneiakja/TIEA2120/VT6/pohja.html