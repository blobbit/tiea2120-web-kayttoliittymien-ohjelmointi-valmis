// voit tutkia tarkemmin käsiteltäviä tietorakenteita konsolin kautta 
// tai json-editorin kautta osoitteessa http://jsoneditoronline.org/
// Jos käytät json-editoria niin avaa data osoitteesta:
// http://appro.mit.jyu.fi/tiea2120/vt/vt1/2018/data.json

// Seuraavilla voit tutkia käytössäsi olevaa tietorakennetta. Voit ohjelmallisesti
// vapaasti muuttaa selaimen muistissa olevan rakenteen sisältöä ja muotoa.

// console.log(data);

// console.dir(data);

// Kirjoita tästä eteenpäin oma ohjelmakoodis

"use strict";

/*
 * EN: I write all of the general functions (as in what I can use in other projects) in english.
 * FIN: Kirjoitan kaikki yleishyodylliset funktiot (eli mita voi kayttaa joskus muissa projekteissa) englanniksi.
 */



// General function to extract fields from array
function fieldsFromArray(array, field) {
    var fields = [];

    for (const i in array)
        fields.push(array[i][field]);

    return fields;
}

// General function to extract objects from array
function objectsFromArray(array, object) {
    var objects = [];

    for (var i = 0; i < array.length; i++)
        for (const o of array[i][object])
            objects.push(o);

    return objects;
}

// Lisaa annetun joukkueen jos loytyy annetun niminen sarja, muuten ei tee mitaan
function lisaaJoukkue(joukkue, sarjanNimi) {
    for (let i of data.sarjat)
        if (i.nimi == sarjanNimi)
            i.joukkueet.push(joukkue);      
}

function poistaJoukkue(poistettavanNimi) {
    for (var i = 0; i < data.sarjat.length; i++)
        for (var j = 0; j < data.sarjat[i].joukkueet.length; j++)
            if (data.sarjat[i].joukkueet[j].nimi == poistettavanNimi)
                data.sarjat[i].joukkueet.splice(j, 1);
}

// Returns an array as a string where elements are separated with the separator
function arrayToString(array, separator) {
    var str = '';

    for (var i = 0; i < array.length; i++) {
        str += array[i];
        if (i < array.length - 1)
            str += separator;
    }

    return str;
}

// General function to remove all elements which do not match regex
function parseArrayWithRegex(array, regex) {
    var newArray = [];

    for (var i = 0; i < array.length; i++)
        if (regex.test(array[i]))
            newArray.push(array[i]);
    
    return newArray;
}

// Palauttaa taulukon missa on kaikki joukkueet pistejarjestyksessa muodossa "nimi (0 p)"
function joukkueetPistejarjestyksessa(toStringMetodi) {
    var joukkueet = [];

    for (let i of objectsFromArray(data.sarjat, 'joukkueet')) {
        var joukkue = new Object();
        joukkue.id = i.id;
        joukkue.rastit = [];
        joukkue.pisteet = 0;
        joukkue.nimi = i.nimi;
        joukkue.aika = 0;

        joukkueet.push(joukkue);
    }

    for (var i = 0; i < data.tupa.length; i++) {
        for (var j = 0; j < data.rastit.length; j++) {
            var tupa = data.tupa[i],
                rasti = data.rastit[j];

            // katsotaan tama ensin, tuvat joilla ei ole edes rastia voimme sivuuttaa kokonaan
            if (tupa.rasti == rasti.id) {
                // etsitaan joukkue jolla on sama id kuin tuvalla
                for (let joukkue of joukkueet) {
                    if (joukkue.id == tupa.joukkue) {
                        if (!joukkue.rastit.includes(rasti)) {
                            joukkue.rastit.push(rasti);
                            if (/^[0-9]/.test(rasti.koodi))
                                joukkue.pisteet += parseInt((rasti.koodi).substring(0, 1));
                            else if (rasti.koodi == 'LAHTO') joukkue.aika -= Date.parse(tupa.aika);
                            else if (rasti.koodi == 'MAALI') joukkue.aika += Date.parse(tupa.aika);
                        } else break;
                    }
                }
            }
        }
    }

    joukkueet.sort(vertaaJoukkueita);

    var joukkueetStringArray = [];

    for (const i in joukkueet)
        joukkueetStringArray.push(toStringMetodi(joukkueet[i]));

    return joukkueetStringArray;
}


// joukkueen tulostaminen taso3 kriteereilla, trim poistaa ylim. valilyonnit nimen lopusta
function joukkueToStringBasic(joukkue) {
    return joukkue['nimi'].trim() + ' (' + joukkue['pisteet'] + ' p)';
}

// joukkueen tulostaminen taso5 kriteereilla
function joukkueToStringAdvanced(joukkue) {
    var matka = 0;

    for (var i = 1; i < joukkue.rastit.length; i++) {
        var rasti1 = joukkue.rastit[i - 1],
            rasti2 = joukkue.rastit[i];

        matka += getDistanceFromLatLonInKm(rasti1.lat, rasti1.lon, rasti2.lat, rasti2.lon);
    }

    var aika ='';

    if (joukkue.aika != undefined)
        aika = ', ' + msToTimeString(joukkue.aika);

    // Jos haluat tismalleen samanlaisen tulosteen kuin tehtavanannossa niin korvaa talla:
    // joukkue['nimi'].trim() + ', ' + joukkue['pisteet'] + ' p, ' + (matka | 0) + ' km' + aika
    return joukkueToStringBasic(joukkue) + ', ' + (matka | 0) + ' km' + aika;
}

// converts milliseconds into format hh:mm:ss, amazingly I couldn't find one ready in Javascript even though Date works with milliseconds, so I copied one from https://coderwall.com/p/wkdefg/converting-milliseconds-to-hh-mm-ss-mmm
function msToTimeString(duration) {
    var milliseconds = parseInt((duration % 1000) / 100)
        , seconds = parseInt((duration / 1000) % 60)
        , minutes = parseInt((duration / (1000 * 60)) % 60)
        , hours = parseInt((duration / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;
}

// compare funktio sorttia varten
function vertaaJoukkueita(a, b) {
    return b.pisteet - a.pisteet;
}

// tehtavaan valmiiksi annetut funktiot etaisyyden laskemiseksi
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

/* 
 * ==============================================
 *                  TASO I
 * ==============================================
 */

// Lisataan uusi joukkue
var uusiJoukkue = {
    "nimi": "Mallijoukkue",
    "last": "2017-09-01 10:00:00",
    "jasenet": [
        "Tommi Lahtonen",
        "Matti Meikäläinen"
    ],
    "id": 99999
}

lisaaJoukkue(uusiJoukkue, '4h');

/*
 * Tulostetaan joukkueiden nimet
 * 
 * 1. objectsFromArray antaa 'joukkueet' - taulukot
 * 2. fieldsFromArray antaa 'nimi' - kentat saaduista taulukoista
 * 3. arrayToString tulostaa saadun 'nimi' taulukon kauniisti laittamalla rivivaihdon alkioiden valiin
 */
console.log(arrayToString(fieldsFromArray(objectsFromArray(data.sarjat, 'joukkueet'), 'nimi'),'\n'));

/*
 * Tulostetaan kokonaisluvulla alkavien rastien koodit
 * 
 * 1. fieldsFromArray antaa 'koodi' - kentat data.rastit taulukosta
 * 2. parseArrayWithRegex poistaa kaikki muut paitsi kokonaisluvulla alkavat alkiot
 * 3. arrayToString tulostaa lopullisen taulukon puolipisteilla eroteltuna
 */
console.log(arrayToString(parseArrayWithRegex(fieldsFromArray(data.rastit, 'koodi'), /^[0-9]/), ';'));

/* 
 * ==============================================
 *                  TASO III
 * ==============================================
 */

poistaJoukkue('Vara 1');
poistaJoukkue('Vara 2');
poistaJoukkue('Vapaat');

console.log(arrayToString(joukkueetPistejarjestyksessa(joukkueToStringBasic), '\n'));

/* 
 * ==============================================
 *                  TASO V
 * ==============================================
 */

// Jos haluat tismalleen samanlaisen tulosteen kuten tehtavanannossa niin kommentoin sen rivin tuonne funktioon joukkueToStringAdvanced
// joukkueToStringAdvanced kutsuu nyt joukkueToStringBasic
console.log(arrayToString(joukkueetPistejarjestyksessa(joukkueToStringAdvanced), '\n'));
