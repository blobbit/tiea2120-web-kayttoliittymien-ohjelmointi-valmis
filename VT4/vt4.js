"use strict";

function luoKuva(src, className) {
    var img = document.createElement('IMG');
    img.src = src;
    img.className = className;
    img.alt = 'kuva ei latautunut!';
    return img;
}

// luo ja palauttaa kuva-elementin annetulla src:lla ja classNamella ja liittaa sen annettuun parenttiin
function luoKuvaParenttiin(parent, src, className) {
    var img = luoKuva(src, className);
    parent.appendChild(img);
    return img;
}

function teePupunPuolisko(x, puoli) {
    var div = document.createElement('div');
    div.className = 'pupunPuolisko ' + puoli;

    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    var pupuHeight = pupu.height / 1.5; // koska jostain syysta pupu ei nay kokonaan ohje videossa...

    const maxVenytys = 40, animSpeed = 0.10;
    var prevTime = 0;
    var venytys = 0;
    var venyyko = true;
    function draw(time) {
        canvas.height = window.innerHeight;
        canvas.width = pupu.width / 2;
        ctx.clearRect(0, 0, canvas.width, canvas.height); // clear canvas

        ctx.save();

        const z = 12; // monta 'aaltoa' pupuun laitetaan
        for (let i = 0; i < z; i++) {
            let pupuY = i * (pupuHeight / z * 2);
            let canvasY = i * (window.innerHeight / z * 2);

            ctx.drawImage(pupu, 0, pupuY, pupu.width, pupuHeight / z, x, canvasY, pupu.width, window.innerHeight / z - venytys);
            ctx.drawImage(pupu, 0, pupuY + pupuHeight / z, pupu.width, pupuHeight / z, x, canvasY + (window.innerHeight / z - venytys), pupu.width, window.innerHeight / z + venytys);
        }

        ctx.restore();

        Math.round(time - prevTime) * animSpeed;
        prevTime = time;

        if (venyyko) venytys++;
        else venytys--;
        if (venytys <= -maxVenytys || venytys >= maxVenytys)
            venyyko = !venyyko;

        requestAnimationFrame(draw);
    }

    requestAnimationFrame(draw);

    div.appendChild(canvas);
    return div;
}

// luodaan pollo satunnaisella liikeradalla
function luoPollo() {
    var pollo = luoKuvaParenttiin(document.getElementById('layer3'), 'http://appro.mit.jyu.fi/tiea2120/vt/vt4/owl.svg', 'pollo');
    pollo.style.animationTimingFunction = pollonLiikeradat[Math.floor(Math.random() * pollonLiikeradat.length)];
}

// tekstiscrollerin luontifunktio. kaytetaan svgpathia (ei ole pakollinen) liikeradan luomiseen
// svg path tuntui olevan ainoa jarkeva toteutustapa
// jostain ihme syysta canvas path2d:sta ei saa otettua paikkaa halutusta kohdasta..
// ajattelin ensin tehda pelkalla matematiikalla mutta sitten tajusin taman
// olisin tehnyt jokaisesta kirjaimesta svg elementin ja kayttanyt SMIL animaatioita mutta piti toteuttaa canvas js animaationa niin tehtiin nyt sitten nain
function teeTekstiScrolleri(canvas, ctx, teksti, animSpeed, offset, path) {
    let moved = 0, pathMoved = 0, // paljon liikutettu
        prevTime = 0; // paljon viime frameen kului aikaa
    const reverseY = canvas.getAttribute('data-reverse-Y') == 'true';
    const margin = 100;
    if (path != undefined) {
        var totalLength = path.getTotalLength(),
            pathWidth = path.getPointAtLength(totalLength).x;
    }

    function getPointFromLoopedPath(length) { // jos length menee yli pathin lengthin niin loopataan pathia
        let x = 0;
        while (length > totalLength) {
            x += pathWidth;
            length -= totalLength;
        }
        let point = path.getPointAtLength(length);
        return { x: point.x + x, y: point.y }
    }

    function draw(time) {
        ctx.clearRect(0, 0, canvas.width, canvas.height); // clear canvas

        ctx.save();

        for (let i = 0, cur = 0; i < teksti.length; i++) {
            let x = canvas.width + cur - moved, y = offset;
            cur += ctx.measureText(teksti[i]).width;

            
            if (x < -margin) { // ei tehda mitaan ruudun ulkopuolella
                if (i == teksti.length - 1) moved = pathMoved = 0; // resettaa scrollerin. ei edes taidettu vaatia
                continue;
            }

            if (path != undefined) {
                const point = getPointFromLoopedPath(pathMoved - cur);
                x -= point.x;
                y = point.y;
                if (reverseY) y *= -1;
                y += offset;
            }

            if (x > canvas.width + margin) continue; // ei tehda mitaan ruudun ulkopuolella

            ctx.fillText(teksti[i], x, y);
        }

        ctx.restore();

        const moveAmount = Math.round(time - prevTime) * animSpeed; // viime framen ajan ja nykyisen framen ajan erotus jotta rullauksesta tulee smoothia
        moved += moveAmount; 
        if (path != undefined) pathMoved += moveAmount * 2;
        prevTime = time;

        requestAnimationFrame(draw);
    }

    requestAnimationFrame(draw);
}

function luoLumihiutale(hiutaleet) {
    var lumihiutale = luoKuvaParenttiin(document.getElementById('layerLumi'), 'http://appro.mit.jyu.fi/tiea2120/vt/vt4/snowflake2.svg', 'lumihiutale'); // ei ole varmaan fiksua hakea lumihiutaletta joka kerta palvelimelta mutta tokkopa tuo paljon rasittaa
    lumihiutale.style.left = Math.floor(Math.random() * window.innerWidth).toString() + 'px';

    const hiutaleKoko = 25; // oletetaan etta kaikki hiutaleet ovat samankokoisia. Oikeasti svg tiedoston koko on 40x30 jostain syysta mutta 25x25 nayttaa paljon fiksummalta

    function tiellaOlevatHiutaleet(x, startIndex, hiutale) {
        // aloitetaan uusimmasta hiutaleesta koska ylempi hiutale on melkeen aina uudempi, mennaan 'alaspain'
        // HUOM: joskus harvoin pari hiutaletta menee paallekkain mutta sita tuskin huomaa ja funktio on nain paljon kevyempi
        for (let i = startIndex - 1; i >= 0; i--)
            if (Math.abs(parseInt(hiutaleet[i].style.left) - parseInt(hiutale.style.left)) < hiutaleKoko)
                return tiellaOlevatHiutaleet(++x, i, hiutaleet[i]);
        return x;
    }

    lumihiutale.style.marginBottom = (hiutaleKoko * tiellaOlevatHiutaleet(0, hiutaleet.length, lumihiutale)).toString() + 'px';

    hiutaleet.push(lumihiutale);
}

// pitaa hakea kuva ennen window.onload funktiota...
const pupu = new Image();
pupu.src = 'http://appro.mit.jyu.fi/tiea2120/vt/vt4/bunny.png';

// pollojen liikerata tarjonta
const pollonLiikeradat = ['linear', 'ease', 'ease-in', 'ease-out', 'ease-in-out', 'linear'];

window.onload = function () {
    // PALKIT
    for (let i = 0; i < 10; i++)
        window.setTimeout(luoKuvaParenttiin, 300 * i, document.getElementById('layer1'), 'palkki.svg', 'palkki');

    // PUPU
    var pupunVasenPuolikas = teePupunPuolisko(0, 'vasen'),
        pupunOikeaPuolikas = teePupunPuolisko(-pupu.width / 2, 'oikea');

    var divider = document.createElement('div');
    divider.className = 'pupuDivider';

    let layer2 = document.getElementById('layer2');
    layer2.appendChild(pupunVasenPuolikas);
    layer2.appendChild(divider);
    layer2.appendChild(pupunOikeaPuolikas);

    // POLLOT
    luoKuvaParenttiin(document.getElementById('layer3'), 'http://appro.mit.jyu.fi/tiea2120/vt/vt4/owl.svg', 'pollo'); // luodaan eka pollo

    var lisaaPollo = document.getElementById('lisaaPollo');
    lisaaPollo.onclick = luoPollo;
    
    // TEKSTISCROLLERIT

    // vaikka kaikkien tiedostojen merkisto pitaisi olla UTF-8, ��kkoset ei silti nay.
    // no, korvasin ne sitten unicode escape sequensseilla
    var teksti = 'TIEA2120 Web-k\u00E4ytt\u00F6liittymien ohjelmointi -kurssin viikkoteht\u00E4v\u00E4 4 taso 3 edellytt\u00E4\u00E4 skrollerin toteuttamista. T\u00E4m\u00E4 skrolleri toimii tekstin m\u00E4\u00E4r\u00E4st\u00E4 riippumatta';
    var canvas = document.getElementById('tekstiScrolleri');
    canvas.width = window.innerWidth;

    var ctx = canvas.getContext('2d');

    ctx.font = '100px Verdana';

    var liukuvari = ctx.createLinearGradient(0, 0, 0, canvas.height);
    liukuvari.addColorStop('0', 'black');
    liukuvari.addColorStop('.5', 'yellow');
    liukuvari.addColorStop('1', 'black');

    ctx.fillStyle = liukuvari;

    teeTekstiScrolleri(canvas, ctx, teksti, 0.05, 100);

    // taso5 tekstiscrolleri
    canvas = document.getElementById('sinusScrolleri');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight / 2;

    ctx = canvas.getContext('2d');
    ctx.font = '30px Verdana';
    ctx.fillStyle = "#FFFFFF";

    teeTekstiScrolleri(canvas, ctx, teksti, 0.3, 300, document.getElementById(canvas.getAttribute('data-path')));

    // LUMISADE
    var hiutaleet = [];
    for (let i = 0; i < 1000; i++) // tuhat on varmaan ihan tarpeeksi...
        window.setTimeout(luoLumihiutale, 500 * i, hiutaleet);
}