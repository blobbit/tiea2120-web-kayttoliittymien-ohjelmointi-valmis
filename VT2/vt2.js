// data-muuttuja on sama kuin viikkotehtävässä 1.
//

"use strict";

console.log(data);

/*
 * EN: I write all of the general functions (as in what I can use in other projects) in english.
 * FIN: Kirjoitan kaikki yleishyodylliset funktiot (eli mita voi kayttaa joskus muissa projekteissa) englanniksi.
 */

/*
 * Tajusin ottaa ainakin seuraavat poikkeustapaukset huomioon:
 * - Ei vaihda valittua rastia jos lisataan uusi rasti kesken joukkueen muokkauksen. (Rastin valinta silti paivittyy)
 * - Voi lisata rastin vaikka kesken leimausten muokkaamista.
 * - Voi olla useita rasteja joilla on sama koodi, rastit yksiloi id, ei koodi. (myos joukkueet yksiloi id)
 * - Lisatyilla rasteilla ja joukkueilla on AINA uniikki ID (siis ihan aina)
 * - Mikali jasenlistauksesta poistetaan keskelta jasen niin listaus paivittyy kauniisti. Eli tyhja jasen on aina lopussa.
 * 
 * HUOM:
 * - Mikali otat joukkueen muokattavaksi joudut painamaan 'lisaa joukkue' ennenkuin voit lisata uuden joukkueen. Eli id jaa aina muistiin vaikka tyhjentaisit kaikki kentat.
 * - Et voi leimata samaa rastia kahdesti (vaikka otaJoukkueet ottaakin sen huomioon)
 * - Koodia pitaa selventaa
 * 
 */



function rastitTekstina() {
    let str = 'RASTIT:\n';

    for (let r of data.rastit)
        str += '(' + 'lon: ' + r.lon + ' | lat: ' + r.lat + ') ' + r.koodi +'\n';

    return str;
}

// creates a html element to the document
function createElement(parent, elementName) {
    var element = document.createElement(elementName);
    parent.appendChild(element);
    return element;
}

// adds a text node to the element
function addText(element, txt) {
    element.appendChild(document.createTextNode(txt));
}

// sarja-sarakkeen sorttaus funktio
// sorttaa 1. sarjan 2. pisteiden 3. nimen mukaan
function sortSarja(a, b) {
    let c = sort(a.sarja, b.sarja);
    if (c != 0) return c;
    c = sort(a.pisteet, b.pisteet);
    if (c != 0) return c;
    return sort(a.nimi, b.nimi);
}

function sortAika(a, b) {
    return aikaErotus(b) - aikaErotus(a);
}

// returns a sort function to sort by given attribute
function sortByAttr(attr) {
    return function (a, b) { return sort(a[attr], b[attr]) };
}

// general sort algorithm to sort strings or numbers
// sorts strings in ascending order (alphabetical)
// sorts numbers in descending order
function sort(a, b) {
    if (typeof a == 'string' && typeof b == 'string') {
        var aa = a.toLowerCase(),
            bb = b.toLowerCase();

        if (aa < bb) return -1;
        if (aa > bb) return 1;
        return 0;
    } else return b-a;
}

/* creates a html table out from array 'objects'.
 * 
 * headers is a object array where each object has 
 *  - header, returns header name
 *  - cellFunc, returns function for creating the cell
 *  - sortFunc, returns function for sorting
 */
function createTableFromArray(headers, objects) {
    var table = document.createElement('table');

    var tr = document.createElement('tr');
    table.appendChild(tr);

    // headers
    for (let header of headers) {
        let th = createElement(tr, 'th');
        addText(th, header.header);
        th.onclick = function (e) {
            e.preventDefault();
            paivitaTaulukko(header.sortFunc);
        };
    }

    // rows
    for (let i = 0; i < objects.length; i++) {
        tr = document.createElement('tr');
        table.appendChild(tr);

        // columns
        for (let header of headers) {
            var td = document.createElement('td');

            header.cellFunc(td, objects[i]);

            tr.appendChild(td);
        }
    }

    return table;
}

// Yleinen funktio jos halutaan tehda simppeli solu
function taulukoi(attr, suffix) {
    if (suffix == undefined) suffix = '';
    return function (parent, object) { addText(parent, object[attr] + suffix); };
}

// Joukkue solun luomiseen tarvitaan oma funktio
function taulukoiJoukkue(parent, joukkue) {
    var a = createElement(parent, 'a');
    a.href = "#joukkue";
    addText(a, joukkue.nimi);
    a.onclick = function () { valitseJoukkue(joukkue); };

    createElement(parent, 'br');
    addText(parent, joukkue.jasenet.join(', '));
}

// suoritetaan kun kayttaja klikkaa joukkueen nimea
function valitseJoukkue(joukkue) {
    valittuJoukkue = joukkue;

    while (jasenKentat.length > Math.max(valittuJoukkue.jasenet.length + 1, 2))
        poistaJasenKentta();

    while (jasenKentat.length < valittuJoukkue.jasenet.length + 1)
        lisaaJasenKentta(jasenKentat.length)

    paivitaJasenKentat();
    paivitaLeimauslistaus();
    paivitaRastinValinta();

    joukkueNimiKentta.value = joukkue.nimi;

    document.getElementById('lisaajoukkue').disabled = false;

    leimauksetSelect.onchange();
}

function taulukoiAika(parent, joukkue) {
    addText(parent, msToTimeString(aikaErotus(joukkue)));
}

// laskee joukkueen kayttaman ajan, ottaa huomioon mikali on aloitettu maalista eli menty rastit takaperin
// eli yksinkertaisesti vahennetaan isommasta pienempi
function aikaErotus(j) {
    if (j.lahtoaika == undefined && j.maaliaika == undefined) return 0;
    return Math.max(j.lahtoaika, j.maaliaika) - Math.min(j.lahtoaika, j.maaliaika);
}

// converts milliseconds into format hh:mm:ss, amazingly I couldn't find one ready in Javascript even though Date works with milliseconds, so I copied one from https://coderwall.com/p/wkdefg/converting-milliseconds-to-hh-mm-ss-mmm
function msToTimeString(duration) {
    if (duration <= 0) return '00:00:00';
    var milliseconds = parseInt((duration % 1000) / 100)
        , seconds = parseInt((duration / 1000) % 60)
        , minutes = parseInt((duration / (1000 * 60)) % 60)
        , hours = parseInt((duration / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;
}

function laskeMatka(joukkue) {
    var matka = 0;

    for (var i = 1; i < joukkue.rastit.size; i++) {
        var rasti1 = Array.from(joukkue.rastit.keys())[i - 1],
            rasti2 = Array.from(joukkue.rastit.keys())[i];

        matka += getDistanceFromLatLonInKm(rasti1.lat, rasti1.lon, rasti2.lat, rasti2.lon);
    }

    return parseInt(matka);
}

// VT1:seen valmiiksi annetut funktiot etaisyyden laskemiseksi
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

function createFieldsetWithLegend(legendtxt, parent) {
    const fieldset = document.createElement('fieldset');
    if (parent != undefined) parent.appendChild(fieldset);
    addText(createElement(fieldset, 'legend'), legendtxt);
    return fieldset;
}

/*
 * var formData = {
 *       parent: parent element,
 *       legend: string,
 *       fields: [ obj, obj ],
 *       button: { name: string, txt: string, onclick: function },
 *       check: { event: , func: }
 *   } 
 */
function createForm(formData) {
    const fieldset = createFieldsetWithLegend(formData.legend, formData.parent);

    // creates form fields
    for (let fieldObj of formData.fields)
        listedElement(fieldset, fieldObj);

    var button = createFormButton(formData.button);
    createElement(fieldset, 'p').appendChild(button);

    formData.parent.addEventListener(formData.check.event, function (e) {
        e.preventDefault();
        button.disabled = formData.check.func(fieldset);
    });

    button.onclick = function (e) {
        e.preventDefault();
        formData.button.onclick();
        if (formData.parent.reset != undefined) formData.parent.reset();
        button.disabled = true;
    };

    return formData.parent;
}

// creates a form input field with label
function createFormInputField(txt, type) {
    let l = document.createElement('label');
    addText(l, txt + ' ');

    let i = createElement(l, 'input');
    i.type = type;
    i.value = '';
    
    return { label:l, input:i };
}

function listedElement(parent, element) {
    const p = document.createElement('p');
    parent.appendChild(p);

    var obj = element;

    for (let x in obj)
        p.appendChild(obj[x]);

    obj.p = p;

    return obj;
}

var leimauksetSelect, rastitSelect;

// generoi osuuden joukkue formista missa muokkaillaan ja lisaillaan rasteja
function rastilistausForm() {
    const fieldset = createFieldsetWithLegend('Leimaukset');

    const p = createElement(fieldset, 'p');
    leimauksetSelect = createElement(p, 'select');

    paivitaLeimauslistaus();

    const poistaleimausButton = createElement(p, 'button');
    addText(poistaleimausButton, 'Poista')
    poistaleimausButton.onclick = function (e) { e.preventDefault(); poistaLeimaus(); };


    const pvmField = listedElement(fieldset, createFormInputField('Pvm', 'date')),
        aikaField = listedElement(fieldset, createFormInputField('Aika', 'time'));

    aikaField.input.step = '1'; // laittamalla step 1 saamme sekuntit nakymaan input kentassa

    const parent = createElement(fieldset, 'p');
    parent.reset = tyhjennaLeimauksenTiedot(pvmField.input, aikaField.input);

    createForm({
        parent: parent,
        legend: 'Leimauksen tiedot',
        fields: [pvmField, aikaField, createRastinValinta(createElement(fieldset, 'p'))],
        button: {
            name: 'TallennaLeimaus', txt: 'Tallenna leimaus', onclick: tallennaLeimaus(pvmField.input, aikaField.input)
        },
        check: { event: 'change', func: function () { return pvmField.input.value == '' || aikaField.input.value == '' || rastitSelect.value == ''; }}
    });

    leimauksetSelect.onchange = paivitaLeimauksenTiedot(pvmField.input, aikaField.input);


    return { fieldset: fieldset };
}

function poistaLeimaus() {
    let i = leimauksetSelect.value;
    if (i == -1) return;
    let key = Array.from(valittuJoukkue.rastit.keys())[i];

    for (let j = 0; j < data.tupa.length; j++)
        if (data.tupa[j].rasti == key.id && data.tupa[j].joukkue == valittuJoukkue.id) {
            data.tupa.splice(j, 1);
            break;
        }

    valittuJoukkue.rastit.delete(key);

    paivitaLeimauslistaus();
    paivitaTaulukko(currentSort);
}

function tyhjennaLeimauksenTiedot(pvmInput, aikaInput) {
    return function () {
        pvmInput.value = '';
        aikaInput.value = '';
        rastitSelect.value = '';
    }
}

function tallennaLeimaus(pvmInput, aikaInput) {
    return function () {
        let rasti;
        for (let r of data.rastit)
            if (r.id == rastitSelect.value) rasti = r;
        if (rasti == undefined) return;

        let date = pvmInput.value + ' ' + aikaInput.value;
        let tupa = { aika: date, rasti: rasti.id, joukkue: valittuJoukkue.id };

        

        if (leimauksetSelect.value == -1 && !valittuJoukkue.rastit.has(rasti)) {
            data.tupa.push(tupa); // lisataan uusi leimaus tietorakenteeseen
        } else {
            for (let t of data.tupa) // muutetaan vanhaa leimausta
                if (t.rasti == rasti.id && t.joukkue == valittuJoukkue.id) {
                    t.aika = date;
                    break;
                }
        }

        valittuJoukkue.rastit.set(rasti, tupa); // paivitetaan valitun joukkueen rastit

        paivitaLeimauslistaus();
        paivitaTaulukko(currentSort);
    }
}

function paivitaLeimauksenTiedot(pvmInput, aikaInput) {
    return function (e) {
        if (e != undefined) e.preventDefault();
        let i = leimauksetSelect.value;
        if (i == -1) {
            tyhjennaLeimauksenTiedot(pvmInput, aikaInput);
            return;
        }
        let key = Array.from(valittuJoukkue.rastit.keys())[i];

        if (key == undefined) return;

        let date = new Date(valittuJoukkue.rastit.get(key).aika);

        pvmInput.value = date.toISOString().substring(0, 10);
        aikaInput.value = date.toTimeString().substring(0, 8);

        rastitSelect.value = key.id;
    }
}

function createRastinValinta() {
    const l = document.createElement('label');
    addText(l, 'Rasti ')
    rastitSelect = document.createElement('select');
    paivitaRastinValinta();
    return { label: l, select: rastitSelect };
}

function paivitaRastinValinta() {
    let value = rastitSelect.value;

    checkAndRemoveChildNodes(rastitSelect);

    for (let r of data.rastit) {
        let option = createElement(rastitSelect, 'option');
        addText(option, r.koodi);
        option.value = r.id;
    }

    rastitSelect.value = value;
}

// paivittaa joukkueformin leimauslistauksen
function paivitaLeimauslistaus() {
    // poistetaan ensin vanhat elementit drop down valikosta
    checkAndRemoveChildNodes(leimauksetSelect);

    // luodaan uudet elementit drop down valikkoon
    let rastit = Array.from(valittuJoukkue.rastit.entries());

    let index = -1;
    uusiLeimausOption('Uusi leimaus...', index++);
    for (var r of rastit)
        uusiLeimausOption(r[0].koodi + ' (' + r[1].aika + ')', index++);
}

function uusiLeimausOption(txt, index) {
    let option = createElement(leimauksetSelect, 'option');
    addText(option, txt);
    option.value = index;
}

function checkAndRemoveChildNodes(parent) {
    if (parent.hasChildNodes()) {
        var vanhalista = parent.childNodes;
        for (let i = vanhalista.length - 1; i >= 0; i--)
            vanhalista[i].remove();
    }
}

var jasenetPohja,
    joukkueNimiKentta,
    jasenKentat = [];

// generoi osuuden joukkue formista missa muokkaillaan ja lisaillaan jasenia
function jasenetForm() {
    jasenetPohja = document.createElement('fieldset');

    addText(createElement(jasenetPohja, 'legend'), 'Jäsenet');

    // otetaan Math.max niin saadaan vahintaan 2 jasen kenttaa
    for (let i = 0; i < Math.max(valittuJoukkue.jasenet.length, 2); i++)
        lisaaJasenKentta(i);

    return { fieldset: jasenetPohja };
}

function lisaaAutomaattisestiJasenKenttia(e) {
    e.preventDefault();

    let tyhjat = 0;

    valittuJoukkue.jasenet = [];

    for (let i = 0; i < jasenKentat.length; i++) {
        if (jasenKentat[i].value == '') {
            tyhjat++;
        } else {
            valittuJoukkue.jasenet.push(jasenKentat[i].value);
        }
    }

    if (tyhjat < 1) lisaaJasenKentta(jasenKentat.length);
    if (tyhjat > 1 && jasenKentat.length > 2) poistaJasenKentta();
}

function lisaaJasenKentta(i) {
    const p = createElement(jasenetPohja, 'p');

    const label = createElement(p, 'label');
    addText(label, 'Jäsen ' + (i + 1) + ' ');

    var input = createElement(label, 'input');
    input.type = 'text';
    input.value = '';

    input.oninput = lisaaAutomaattisestiJasenKenttia;

    jasenKentat.push(input);
}

function poistaJasenKentta() {
    jasenKentat.pop().parentElement.remove();
    paivitaJasenKentat();
}

function paivitaJasenKentat() {
    for (let i = 0; i < jasenKentat.length; i++) {
        if (i < valittuJoukkue.jasenet.length)
            jasenKentat[i].value = valittuJoukkue.jasenet[i];
        else jasenKentat[i].value = '';
    }
}


// creates a form button
function createFormButton(buttonData) {
    var button = document.createElement('button');
    button.name = buttonData.name;
    button.id = buttonData.name;
    addText(button, buttonData.txt);
    button.disabled = true;

    return button;
}

/*
 * Kopoitu lahes kokonaan VT1
 * VT2: joukkue objekteille lisataan myos tieto jasenista ja sarjasta
 */
function otaJoukkueet() {
    var joukkueet = [];

    for (let sarja of data.sarjat) {
        for (let i of sarja.joukkueet) {
            var joukkue = new Object();

            joukkue.id = i.id;
            joukkue.sarja = sarja.nimi;
            joukkue.rastit = new Map();
            joukkue.pisteet = 0;
            joukkue.nimi = i.nimi;
            joukkue.lahtoaika = 0;
            joukkue.maaliaika = 0;
            joukkue.jasenet = i.jasenet;

            joukkueet.push(joukkue);
        }
    }

    for (var i = 0; i < data.tupa.length; i++) {
        for (var j = 0; j < data.rastit.length; j++) {
            var tupa = data.tupa[i],
                rasti = data.rastit[j];

            // katsotaan tama ensin, tuvat joilla ei ole edes rastia voimme sivuuttaa kokonaan
            if (tupa.rasti == rasti.id) {
                // etsitaan joukkue jolla on sama id kuin tuvalla
                for (let joukkue of joukkueet) {
                    if (joukkue.id == tupa.joukkue) {
                        if (!joukkue.rastit.has(rasti)) {
                            joukkue.rastit.set(rasti, tupa);
                            if (/^[0-9]/.test(rasti.koodi))
                                joukkue.pisteet += parseInt((rasti.koodi).substring(0, 1));
                            else if (rasti.koodi == 'LAHTO') joukkue.lahtoaika = Date.parse(tupa.aika);
                            else if (rasti.koodi == 'MAALI') joukkue.maaliaika = Date.parse(tupa.aika);
                        } else break;
                    }
                }
            }
        }
    }

    for (let j of joukkueet)
        j.matka = laskeMatka(j);

    return joukkueet;
}

function lisaaRasti(latInput, lonInput, koodiInput) {
    return function () {
        var lat = latInput.value,
            lon = lonInput.value,
            koodi = koodiInput.value;

        if (lat == '' || lon == '' || koodi == '') return false;

        const uusiRasti = {
            kilpailu: data.kisa,
            lon: lon,
            koodi: koodi,
            lat: lat,
            id: generoiID(checkIDRasti)
        };

        data.rastit.push(uusiRasti);
        paivitaRastinValinta();

        console.log(rastitTekstina());
    }
}

var valittuJoukkue = uusiLisattavaJoukkue();
// oletus sarja johon uusi joukkue lisataan
const oletusSarja = 1;
function uusiLisattavaJoukkue() {
    return {
        "nimi": '',
        "last": "2017-09-01 10:00:00",
        "jasenet": [],
        "id": generoiID(checkIDJoukkue),
        "rastit": new Map()
    };
}

// generoi uuden id:n
// checkfunc on kaytannossa turha koska uuden id:n mahdollisuuksia on niin paljon mutta tulipahan tehtya
function generoiID(checkFunc) {
    let random;
    while (checkFunc(random = parseInt(Math.random() * Number.MAX_SAFE_INTEGER)));
    return random;
}

// tarkistaa onko jollain joukkueella jo sama id luotaessa uutta joukkuetta
function checkIDJoukkue(random) {
    for (let i of data.sarjat)
        for (let j of i.joukkueet)
            if (j.id == random) return true;
    return false;
}

// tarkistaa onko jollain rastilla jo sama id luotaessa uutta rastia
function checkIDRasti(random) {
    for (let r of data.rastit)
        if (r.id == random) return true;
    return false;
}

// muokkaa tai lisaa joukkueen
function tallennaJoukkue() {
    lisaaTaiMuokkaaJoukkueDataan(valittuJoukkue);
    valittuJoukkue = uusiLisattavaJoukkue();

    paivitaTaulukko(currentSort);
    paivitaLeimauslistaus();

    // Poistetaan ylim. jasenkentat
    while (jasenKentat.length > 2)
        poistaJasenKentta();
}

// mikali joukkue samalla id:lla loytyy niin korvataan se, muuten lisataan joukkue oletussarjaan
function lisaaTaiMuokkaaJoukkueDataan(joukkue) {
    for (let i of data.sarjat) {
        for (let j of i.joukkueet) {
            if (j.id == joukkue.id) {
                j.nimi = joukkue.nimi;
                j.jasenet = joukkue.jasenet;
                return;
            }
        }
    }
    data.sarjat[oletusSarja].joukkueet.push(joukkue);
}

// tarkistaa etta kaikissa uuden rastin input kentissa on tekstia
function tarkistaUusiRasti(parent) {
    for (let input of parent.getElementsByTagName('input'))
        if (input.value == '') return true;
    return false;
}

// tarkistaa etta lisattavalla joukkueella on nimi ja vah. 2 jasenta
function tarkistaUusiJoukkue() {
    valittuJoukkue.nimi = joukkueNimiKentta.value;
    return !(valittuJoukkue.nimi != '' && valittuJoukkue.jasenet.length >= 2);
}

// poistetaan vanha taulukko (jos on) ja tehdaan uusi tilalle
function paivitaTaulukko(sortFunc) {
    if (table != undefined) table.remove();
    table = createTableFromArray(headers, otaJoukkueet().sort(currentSort = sortFunc));
    document.getElementById('tupa').appendChild(table);
}

// tulospalvelu taulukko
// tama syotetaan taulukonluonti funktiolle yhdessa kasiteltavan datan kanssa (joukkueet)
// jokainen sarake on oma objekti jolla on sarakeotsikko (header), solunluontifunktio (cellFunc), sorttausfunctio (sortFunc)
const headers = [
    { header: 'Sarja', cellFunc: taulukoi('sarja'), sortFunc: sortSarja },
    { header: 'Joukkue', cellFunc: taulukoiJoukkue, sortFunc: sortByAttr('nimi') },
    { header: 'Pisteet', cellFunc: taulukoi('pisteet',' p'), sortFunc: sortByAttr('pisteet') },
    { header: 'Matka', cellFunc: taulukoi('matka',' km'), sortFunc: sortByAttr('matka') },
    { header: 'Aika', cellFunc: taulukoiAika, sortFunc: sortAika }
];

var table, currentSort = sortSarja;

window.onload = function () {    
    // piirretaan taulukko ensimmaista kertaa
    paivitaTaulukko(currentSort);

    const nimiInputField = createFormInputField('Nimi', 'text');
    joukkueNimiKentta = nimiInputField.input;

    const lat = createFormInputField('Lat', 'text'),
        lon = createFormInputField('Lon', 'text'),
        koodi = createFormInputField('Koodi', 'text');


    var forms = document.querySelectorAll('form');

    // rastin lisays form
    const rastiFormData = {
        parent: forms[0],
        legend: 'Rastin tiedot',
        fields: [lat, lon, koodi],
        button: { name: 'rasti', txt: 'Lisää rasti', onclick: lisaaRasti(lat.input, lon.input, koodi.input) },
        check: { event:'input', func: tarkistaUusiRasti }
    };

    // joukkueen lisays / muokkaus form
    const joukkueFormData = {
        parent: forms[1],
        legend: 'Joukkueen tiedot',
        fields: [nimiInputField, jasenetForm(), rastilistausForm()],
        button: { name: 'lisaajoukkue', txt: 'Tallenna joukkue', onclick: tallennaJoukkue },
        check: { event:'input', func: tarkistaUusiJoukkue }
    };

    createForm(rastiFormData);

    createForm(joukkueFormData);
}