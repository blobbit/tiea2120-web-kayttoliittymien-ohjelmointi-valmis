"use strict";

// palauttaa id:ta vastaavan sarjan nimen. Jos ei loydy niin palauttaa tyhjan
function getSarjaNimiByID(id) {
    for (let i of data.kisat[0].sarjat)
        if (i.id == id) return i.nimi;
    return '';
}

// palauttaa koordinaatin pyoristettyna 6 desimaalin tarkkuudelle, koska tietorakenteessa koordinaatit esitetaan nain
function roundCoordinate(i) {
    return parseFloat((Math.round(i * 1000000) / 1000000).toFixed(6));
}

// palauttaa id:ta vastaavan rastin. Jos ei loydy niin palauttaa tyhjan rastin
function getRastiByID(rastit, id) {
    for (let i of rastit)
        if (i.id == id) return i;
    return { koodi: '' };
}

function laskePisteetJaMatka(rastit, joukkue) {
    let matka = 0, pisteet = 0;

    for (let i = 0, rasti1 = null; i < joukkue.rastit.length; i++) {
        let rasti2 = getRastiByID(rastit, joukkue.rastit[i].id);

        if (/^[0-9]/.test(rasti2.koodi))
            pisteet += parseInt((rasti2.koodi).substring(0, 1)); // substring koska toinenkin merkki voi olla numero

        if (rasti1 != null)
            matka += getDistanceFromLatLonInKm(rasti1.lat, rasti1.lon, rasti2.lat, rasti2.lon);

        rasti1 = rasti2;
    }

    return '(' + pisteet + ' p, ' + matka.toFixed(1) + ' km)';
}

// VT1:seen valmiiksi annetut funktiot etaisyyden laskemiseksi
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

// tarkistaa etta uudella joukkueella on vahintaan 2 jasenta
const tarkastaJasenKentat = () => {
    let kentat = document.getElementsByClassName('jasen');

    let taytetytKentat = 0;
    for (let kentta of kentat)
        if (kentta.value != '') taytetytKentat++;

    if (taytetytKentat < 2) {
        kentat[kentat.length - 1].setCustomValidity('Jasenia pitaa olla v\u00E4hint\u00E4\u00E4n 2 !'); // toimii kylla aakkosillakin koska charset on utf mutta varmuuden vuoksi unicodea
        return false;
    }
    else
        for (let kentta of kentat)
            kentta.setCustomValidity('');
    return true;
};

// tarkistaa etta vahintaan yksi leimaustapa on valittuna
const tarkastaLeimaustavat = () => {
    let leimaustavat = document.getElementsByClassName('leimaustapa');

    if (leimaustavat == undefined || leimaustavat.length == 0) return;

    for (let leimaustapa of leimaustavat)
        if (leimaustapa.checked) {
            for (let i of leimaustavat)
                i.setCustomValidity('');
            return true;
        }

    leimaustavat[leimaustavat.length - 1].setCustomValidity('V\u00E4hint\u00E4\u00E4n yksi leimaustapa pit\u00E4\u00E4 olla valittuna !');
    return false;
};

// funktiopohjainen komponentti LisaaJoukkue
// ottaa parametreina leimaustavat ja sarjat
// palauttaa lomakkeen jolla voi lisata joukkueita
function LisaaJoukkue(props) {
    
    let leimaustavat = [];
    for (let i of props.leimaustavat) {
	    let id = 'leimaustavat' + i.toLowerCase();
	    leimaustavat.push( <li key={i}>
            <label htmlFor={id}>{i}</label>
            <input type='checkbox' className='leimaustapa' id={id} value={i} checked={props.leimaustapa.includes(i)} onChange={props.leimaustavatChange} />
			        </li> );
    }

    let sarjat = [];
    for (let i of props.sarjat) {
	    let id = 'sarjat' + i.nimi.toLowerCase();
	    sarjat.push( <li key={i.nimi}>
                    <label htmlFor={id}>{i.nimi}</label>
                    <input type='radio' id={id} value={i.id} name='sarjat' checked={props.sarja == i.id} onChange={props.sarjatChange} />
		            </li>);
    }
    
    let jasenet = [];
    for (let i = 0; i < props.jasenet.length; i++) {
	    let id = i + 'jasen';
	    jasenet.push(<div key={id}>
            <label htmlFor={id}>J&auml;sen {i + 1}</label>
            <input type='text' id={id} className='jasen kentta' onChange={props.jasenetChange} value={ props.jasenet[i] } />
            </div>);
    }

    let luontiaika = '';
    if (props.luontiaika != null) luontiaika = props.luontiaika.replace(' ', 'T');


    return <div className = 'lomake'>
	<h2>Lis&auml;&auml; joukkue</h2>
	<label id='joukkueLisatty' className='palaute'></label>

	<form method='post' id='joukkueForm' onSubmit={props.onSubmit}>
        
	<fieldset className='tiedot'>
            <legend>Joukkueen tiedot</legend>
            <div>
                <label htmlFor='nimi'>Nimi</label>
                <input type='text' id='nimi' className='kentta' required minLength='2' value={props.nimi} onChange={props.nimiChange}/>
            </div>
            <div>
                    <label htmlFor='luontiaika'>Luontiaika</label>
                    <input type='datetime-local' id='luontiaika' className='kentta' step='0.001' value={ luontiaika } onChange={props.luontiaikaChange} />
            </div>
            <div>
                <label>Leimaustapa</label>
                <ul className='kentta' id='leimaustavat'>
	            { leimaustavat }
                </ul>
            </div>
            <div>
                <label>Sarja</label>
                <ul className='kentta' id='sarjat'>
	            { sarjat }
                </ul>
            </div> 
        </fieldset>

        <fieldset className='tiedot' id='jasenetListaus'>
             <legend>J&auml;senet</legend>
	{ jasenet }
        </fieldset>

	<input type='submit' value='Tallenna' />
	</form>
    </div>;
}

// funktio komponentti joukkueen jasenlistaukselle
const JasenListaus = (props) => {
    let jasenet = [];
    for (let i of props.jasenet) {
        if (i == '') continue; // muokkaus vaiheessa ei nayteta tyhjia
        jasenet.push(<li key={props.joukkueID + ' jasen' + jasenet.length.toString()}>{i}</li>);
    }

    return <ul>{ jasenet }</ul>;
};

// funktio komponentti yksittaisen joukkueen tiedoille
const JoukkueenTiedot = (props) => <div>
        <a id={props.joukkue.id} href='#joukkueForm' onClick={props.muokkaaJoukkuetta}>{props.joukkue.nimi}</a> {laskePisteetJaMatka(props.rastit, props.joukkue)}<br />
        {getSarjaNimiByID(props.joukkue.sarja) + ' (' + props.joukkue.leimaustapa.toString() + ')'}<br />
        <JasenListaus joukkueID={props.joukkue.id} jasenet={props.joukkue.jasenet} />
</div>;

// funktio komponentti joka listaa joukkueet
const ListaaJoukkueet = (props) => {
    let joukkueet = [];
    for (let i of props.joukkueet)
        joukkueet.push(<li key={i.id}>
            <JoukkueenTiedot joukkue={i} rastit={props.rastit} muokkaaJoukkuetta={props.muokkaaJoukkuetta} />
        </li>);

    return <div>
        <h2>Joukkueet</h2>
        <b>Joukkueita voi muokata linkistä.</b>
        <ul> {joukkueet} </ul>
    </div>;
};

// funktio komponentti joka listaa rastit
const ListaaRastit = (props) => {
    let rastit = [];
    for (let r of props.rastit)
        rastit.push(<RastinTiedot key={r.id} rasti={r} muutaRastia={props.muutaRastia} />);

    return <div>
        <h2>Rastit</h2>
        <b>Rastien nimiä ja koordinaatteja voi muuttaa klikkaamalla.</b>
        <ul>
            {rastit}
        </ul>
    </div>;
};

// funktio komponentti joka renderoi rastin koodin rastilistauksessa
const RastinKoodi = (props) => {
    const validation = e => {
        let input = e.target;

        if (input.value.match(/^(\d.|LAHTO|MAALI)$/g)) { // koodin pitaa alkaa numerolla ja olla 2 merkkia pitka, poikkeuksena LAHTO tai MAALI
            input.setCustomValidity('');
            props.event(e);
        } else {
            input.setCustomValidity('Koodin pit\u00E4\u00E4 alkaa numerolla ja olla 2 merkki\u00E4 pitk\u00E4 ! (LAHTO tai MAALI kelpaa my\u00F6s)');
            input.reportValidity(); // jostain syysta error message ei aina nay TODO: fix
        }
    };

    if (props.muokataanko)
        return <input onBlur={validation} defaultValue={props.koodi} autoFocus={true}/>;
    else return <p onClick={props.event}>{props.koodi}</p>;
};

// komponentti joka renderoi rastin tiedot rastilistauksessa
class RastinTiedot extends React.Component {
    muutaRastia; // tahan sailotaan funktio app komponentista jotta voimme halutessamme kutsua ja paivittaa sen tilaa

    constructor(props) {
        super(props);

        this.state = {
            'rasti': props.rasti,
            'muokataankoKoodia': false,
            'muokataankoKoordinaatteja': false
        };

        this.muokkaaKoodia = this.muokkaaKoodia.bind(this);
        this.muokataankoKarttaa = this.muokataankoKarttaa.bind(this);
        this.avaaKartta = this.avaaKartta.bind(this);
        this.muutaRastia = props.muutaRastia;
    }

    muokkaaKoodia(e) {
        let newState = { 'muokataankoKoodia': !this.state.muokataankoKoodia };

        if (e.target.nodeName == 'INPUT') {
            let rasti = this.state.rasti;
            rasti.koodi = e.target.value;
            newState.rasti = rasti;
            this.muutaRastia();
        }

        this.setState(newState);
    }

    muokataankoKarttaa() {
        this.setState({ 'muokataankoKoordinaatteja': !this.state.muokataankoKoordinaatteja });
    }

    avaaKartta(e) {
        if (document.getElementById('kartta') != undefined) return; // jos kartta on jo olemassa

        const caller = e.target;

        const mapContainer = document.createElement('div');
        mapContainer.id = 'kartta';
        caller.appendChild(mapContainer);

        const map = L.map(mapContainer, {
            crs: L.TileLayer.MML.get3067Proj()
        });
        L.tileLayer.mml_wmts({ layer: 'maastokartta' }).addTo(map);

        let lat = this.state.rasti.lat, lon = this.state.rasti.lon;
        let marker = L.marker([lat, lon], { draggable: true, autoPan: true }).addTo(map);
        map.setView([lat, lon], 10); // 10 is the zoom level

        $(document).on('mouseup', e => { 
            var kartta = $("#kartta");

            // onblur ei toimi karttojen kanssa niin tehdaan nain etta kuunnellaan kaikki klikkaukset ja katsotaan klikattiinko karttaa vai jotain muuta
            // poistetaan kartta ja otetaan koordinaatit talteen
            if (!kartta.is(e.target) && kartta.has(e.target).length === 0) {
                let latLng = marker.getLatLng();

                let rasti = this.state.rasti;
                rasti.lat = roundCoordinate(latLng.lat);
                rasti.lon = roundCoordinate(latLng.lng);
                this.setState({ 'rasti': rasti });
                this.muutaRastia();

                kartta.remove();
                $(document).off('mouseup');
            }
        });
    }

    render() {
        let r = this.state.rasti;

        return <li>
            <RastinKoodi muokataanko={this.state.muokataankoKoodia} koodi={r.koodi} event={this.muokkaaKoodia} />
            <p onClick={this.avaaKartta} >{r.lat + ', ' + r.lon}</p>
        </li>;
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);

        // tehdään kopio tietorakenteen joukkueista
        // Tämä on tehtävä näin, että saadaan oikeasti aikaan kopio eikä vain viittausta samaan tietorakenteeseen. Objekteja ja taulukoita ei voida kopioida vain sijoitusoperaattorilla
        // päivitettäessä React-komponentin tilaa on aina vanha tila kopioitava uudeksi tällä tavalla
        // kts. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from
        let joukkueet = Array.from(data.joukkueet, function (j) {
            // luodaan uusijoukkue
            let uusij = {};
            // kopioidaan tavalliset kentät
            let kentat = ['nimi', 'sarja', 'seura', 'id', 'luontiaika'];
            for (let i of kentat)
                uusij[i] = j[i];
            // taulukot on kopioitava erikseen. Nyt riittää pelkkä array.from, koska tauluiden
            // sisällä ei ole muita taulukoita tai objekteja
            let uusijasenet = Array.from(j["jasenet"]);
            let uusirastit = Array.from(j["rastit"]);
            let uusileimaustapa = Array.from(j["leimaustapa"]);
            uusij["jasenet"] = uusijasenet;
            uusij["rastit"] = uusirastit;
            uusij["leimaustapa"] = uusileimaustapa;
            return uusij;
        });
        
        console.log(joukkueet);

        let rastit = Array.from(data.rastit); // tehdaan rasteista kanssa kopio

        let kisa = data.kisat[0];

        let leimaustavat = new Map();
        for (let leimaustapa of kisa.leimaustapa) {
            leimaustavat.set(leimaustapa, false);
        }

        this.state = {
            'joukkueet': joukkueet,
            'rastit': rastit,
            'leimaustavat': kisa.leimaustapa,
            'sarjat': kisa.sarjat,
            'joukkue': this.uusiJoukkue()
        };

        // sitominen
        this.nimiChange = this.nimiChange.bind(this);
        this.luontiaikaChange = this.luontiaikaChange.bind(this);
        this.leimaustavatChange = this.leimaustavatChange.bind(this);
        this.jasenetChange = this.jasenetChange.bind(this);
        this.sarjatChange = this.sarjatChange.bind(this);
        this.muokkaaJoukkuetta = this.muokkaaJoukkuetta.bind(this);
        this.tallennaJoukkue = this.tallennaJoukkue.bind(this);
        this.checkIDJoukkue = this.checkIDJoukkue.bind(this);
        this.muutaRastia = this.muutaRastia.bind(this);
    }

    // palauttaa tyhjan joukkueen
    uusiJoukkue() {
        return {
            nimi: '',
            jasenet: ['', ''],
            sarja: '4751022794735616',
            seura: null,
            id: 0,
            rastit: [],
            pisteet: 0,
            matka: 0,
            leimaustapa: [],
            luontiaika: ''
        };
    }

    // generoi uuden id:n
    // checkfunc on kaytannossa turha koska uuden id:n mahdollisuuksia on niin paljon mutta tulipahan tehtya
    generoiID(checkFunc) {
        let random;
        while (checkFunc(random = parseInt(Math.random() * Number.MAX_SAFE_INTEGER)));
        return random;
    }

    // tarkistaa onko jollain joukkueella jo sama id luotaessa uutta joukkuetta
    checkIDJoukkue(random) {
        for (let i of this.state.joukkueet)
            if (i.id == random) return true;
        return false;
    }

    // kasittelee nimen muutoksen lomakkeessa
    nimiChange(e) {
        let joukkue = this.state.joukkue;
        joukkue.nimi = e.target.value;
        this.setState({
            'joukkue': joukkue
        });
    }

    // kasittelee luontiajan muutokset lomakkeessa
    luontiaikaChange(e) {
        let joukkue = this.state.joukkue;
        joukkue.luontiaika = e.target.value.replace('T', ' '); // koska jostain ihme syysta tietorakenne ei kayta iso standardia, se olisi liian helppoa
        this.setState({
            'joukkue': joukkue
        });
    }

    // kasittelee leimaustapojen muutokset lomakkeessa
    leimaustavatChange(e) {
        let joukkue = this.state.joukkue,
            hasLeimaustapa = joukkue.leimaustapa.includes(e.target.value);

        if (e.target.checked && !hasLeimaustapa) joukkue.leimaustapa.push(e.target.value); // jos chekataan niin lisataan leimaustapa
        else if (!e.target.checked && hasLeimaustapa) { // jos unchekataan niin poistetaan leimaustapa
            for (let i = 0; i < joukkue.leimaustapa.length; i++) {
                if (joukkue.leimaustapa[i] == e.target.value) {
                    joukkue.leimaustapa.splice(i, 1);
                    break;
                }
            }
        }

        this.setState({
            'joukkue': joukkue
        });
    }

    jasenetChange(e) {
        let joukkue = this.state.joukkue,
            jasenet = joukkue.jasenet;
        jasenet[parseInt(e.target.id)] = e.target.value;

        let tyhjat = 0;
        for (let j of jasenet)
            if (j == '') tyhjat++;

        if (tyhjat == 0 && jasenet.length < 5) jasenet.push(''); // lisataan kenttia tarpeen mukaan, ei kuitenkaan enempaa kuin 5

        

        this.setState({
            'joukkue': joukkue
        });
    }

    // kasittelee sarjojen muutokset lomakkeessa
    sarjatChange(e) {
        let joukkue = this.state.joukkue;

        if (e.target.checked)
            joukkue.sarja = e.target.value

        this.setState({
            'joukkue': joukkue
        });
    }

    muokkaaJoukkuetta(e) {
        this.resetoiFormi();    

        let joukkue;
        for (let i of this.state.joukkueet) {
            joukkue = i;
            if (i.id == e.target.id) break;
        }

        if (joukkue.jasenet.length < 5) joukkue.jasenet.push('');

        this.setState({
            'joukkue': joukkue
        });
    }

    // tehdaan validation checkit
    componentDidUpdate() {
        tarkastaLeimaustavat();
        tarkastaJasenKentat();
    }

    // tallentaa uuden joukkueen ja lisaa sen joukkuelistaukseen
    tallennaJoukkue(e) {
        e.preventDefault();

        let joukkueet = this.state.joukkueet;

        let joukkue = this.state.joukkue;

        if (joukkue.id == 0) {
            joukkue.id = this.generoiID(this.checkIDJoukkue);
            joukkueet.push(joukkue);
        }
            

	    this.setState({
            'joukkueet': joukkueet
        });

        this.resetoiFormi();
    }

    resetoiFormi() {
        this.setState({
            'joukkue': this.uusiJoukkue()
        });
    }

    muutaRastia(r) {
        this.setState({'rastit': this.state.rastit });
    }

    render() {
      return <div className='container'>
	    <LisaaJoukkue
	          leimaustavat={this.state.leimaustavat}
	          sarjat={this.state.sarjat}
              onSubmit={this.tallennaJoukkue}

              nimi={this.state.joukkue.nimi}
              luontiaika={this.state.joukkue.luontiaika}
              leimaustapa={this.state.joukkue.leimaustapa}
              sarja={this.state.joukkue.sarja}
              jasenet={this.state.joukkue.jasenet}

	          leimaustavatChange={this.leimaustavatChange}
              sarjatChange={this.sarjatChange}
              jasenetChange={this.jasenetChange}
              nimiChange={this.nimiChange}
              luontiaikaChange={this.luontiaikaChange}
          />

          <ListaaJoukkueet joukkueet={this.state.joukkueet} rastit={this.state.rastit} muokkaaJoukkuetta={this.muokkaaJoukkuetta} />

          <ListaaRastit rastit={this.state.rastit} muutaRastia={this.muutaRastia} />
	    </div>;
    }
}


ReactDOM.render(
    <App />,
    document.getElementById('root')
);

tarkastaJasenKentat();
tarkastaLeimaustavat();